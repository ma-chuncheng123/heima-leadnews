package com.heima.utils.common;


public class Base64Utils {

    /**
     * 解码
     *
     * @param base64
     * @return
     */
    public static byte[] decode(String base64) {
        try {
            // Base64解码
            byte[] b = org.springframework.util.Base64Utils.decode(base64.getBytes());
            for (int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {// 调整异常数据
                    b[i] += 256;
                }
            }
            return b;
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * 编码
     *
     * @param data
     * @return
     * @throws Exception
     */
    public static String encode(byte[] data) {
        return org.springframework.util.Base64Utils.encodeToString(data);
    }
}